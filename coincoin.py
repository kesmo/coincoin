#!/usr/bin/python3

import datetime, os, argparse, json, requests, time, hashlib, base64, random, difflib, shutil
import subprocess, gzip
import sys, logging, traceback

from lxml import html
from urllib.parse import urlparse

from sqlalchemy import create_engine, func
from sqlalchemy.orm import Session

from sqlalchemy_declarative import Advert, AdvertImage, AdvertChange, AdvertChangeTypeEnum, VpnServer

from mullvad import get_mullvad_servers, mullvad_set_relay_hostname, mullvad_connect, mullvad_disconnect

#Immobilier
COINCOIN_CATEGORY_IMMO = 9
COINCOIN_SEARCH = ''

COINCOIN_IMMO_TYPE = '' #{'old', 'new', 'viager'}
COINCOIN_IMMO_PRICE = '' #'min-500000'
COINCOIN_IMMO_SIZE = '' #'70-min'
COINCOIN_IMMO_ESTATE = '' # {'1': 'maison', '2': 'appartement', '3': 'terrain', '4': 'parking', '5': 'autre'}

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0'
http_session = requests.session()
http_session.headers.update({'User-Agent': USER_AGENT})

COOKIES_FILE = os.path.join(BASE_DIR, 'cookies')

# seloger
# [{%22postalCodes%22:[%2269390%22]},{%22inseeCodes%22:[690027]},{%22inseeCodes%22:[690046]},{%22inseeCodes%22:[690051]},
# {%22inseeCodes%22:[690136]},{%22inseeCodes%22:[690141]},{%22inseeCodes%22:[690179]},{%22inseeCodes%22:[690219]},
# {%22inseeCodes%22:[690237]},{%22inseeCodes%22:[690241]},{%22inseeCodes%22:[690268]},{%22inseeCodes%22:[690048]},
# {%22inseeCodes%22:[690133]},{%22inseeCodes%22:[690148]},{%22inseeCodes%22:[690176]},{%22inseeCodes%22:[690249]}]

# leboncoin.fr
# Soucieu-en-Jarrest_69510__45.67758_4.70232_4639
# Taluyers_69440__45.63912_4.7236_2607
# Montagny_69700__45.6207_4.74748_3403
# Vourles_69390__45.6598_4.77363_2695
# Saint-And%C3%A9ol-le-Ch%C3%A2teau_69700__45.58425_4.69344_5707
# Beauvallon_69700__45.58425_4.69344_5707
# Saint-Laurent-d%27Agny_69440__45.6408_4.68562_3112
# Chaussan_69440__45.63325_4.63907_2687
# Brignais_69530__45.67405_4.75395_2695
# Mornant_69440__45.6195_4.67158_3457

POSTAL_CODES = {
    'lyon': list(range(69001, 69010)),
    'paris': list(range(75001, 75021)),
    'mornant': { 69510, 69440, 69700, 69390, 69530 }
}

BLACKLISTED_VPN_SERVERS_FILE = 'blacklisted_vpn_servers.json'

logger = logging.getLogger()
str_handler = logging.StreamHandler(sys.stdout)
str_handler.setLevel(logging.INFO)
str_formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
str_handler.setFormatter(str_formatter)
logger.addHandler(str_handler)
logger.setLevel(logging.DEBUG)

blacklisted_servers = []
if os.path.isfile(BLACKLISTED_VPN_SERVERS_FILE):
     json.loads(open(BLACKLISTED_VPN_SERVERS_FILE, 'rt').read())

class AdRequestException(Exception):
    pass

def download_file(folder, url, download):
    local_filename = os.path.join(folder, os.path.basename(urlparse(url).path))
    if not os.path.exists(local_filename):
        if not download:
            return None
        if not os.path.exists(folder):
            os.mkdir(folder)
        logger.debug('Downloading %s' % url)
        try:
            resp = http_session.get(url, timeout=30)
        except Exception as e:
            logger.debug('Not able to request %s. An exceotion occured: %s' % (url, e))
            return None

        if resp.status_code != 200:
            logger.warning('Not able to retrieve image %s. Error: %s' % (url, resp.status_code))
            return None

        open(local_filename, 'wb').write(resp.content)
        checksum = hashlib.sha256(resp.content).hexdigest()
    else:
        checksum = hashlib.sha256(open(local_filename, 'rb').read()).hexdigest()
    return checksum

def find_json_ads(api_key, referer, category, postal, search='', immo_type='', price='', square=''):
    url = 'https://api.leboncoin.fr/finder/search'

    resp = http_session.options(url, headers={
        'Access-Control-Request-Headers': 'api_key,content-type',
        'Access-Control-Request-Method': 'POST',
        'Origin': 'https://www.leboncoin.fr',
        'Referer': referer,
        'Cache-Control': 'no-cache',
    })
    if resp.status_code != 200 and resp.status_code != 204:
        raise AdRequestException('Not able to request %s. Error: %s' % (url, resp.status_code))

    logger.info('Request options %s (%s)' % (url, resp.headers))

    json_request = {
        "filters": {
            "category": {"id": category},
            "enums": {
                "ad_type":["offer"],
                #"immo_sell_type":[immo_type]
            },
            "location": {
                "locations": [
                    {
                        "zipcode": postal,
                        # "region_id": "22",
                        # "department_id": "69"
                    }
                ]
            },
            "ranges": {
                #"price":{"max":price}
            }
        },
        "limit": "100",
        "limit_alu": "0",
        "owner_type": "all",
        "sort_by": "time",
        "sort_order": "desc"
    }

    # if immo_type:
    #     json_request['filters']['enums']['immo_sell_type'] = [immo_type]

    # if price:
    #     json_request['filters']['ranges']['price'] = {'max': price}

    # if square:
    #     json_request['filters']['ranges']['square'] = {'min': price}

    logger.debug('Request %s' % url )
    data = http_session.post(url, json=json_request, headers={
        'Api_key': api_key,
        'Origin': resp.headers['Access-Control-Allow-Origin'],
        'Referer': referer,
        'Content-type': 'application/json',
        'Cache-Control': 'no-cache',
    })

    time.sleep(random.uniform(5.0, 10.0))
    if data.status_code == 200:
        return data.json()
    else:
        raise AdRequestException('Not able to retrieve json advertisements from %s. Error: %s' % (url, data.status_code, data.headers))

def find_ads(args, **kwargs): #category, postal, page, search='', immo_type='', immo_price='', immo_size='', immo_estate=''):
    url = 'https://www.leboncoin.fr/recherche?category=%s&locations=%s&page=%s' % (kwargs['category'], kwargs['postal'], kwargs['page'])
    if kwargs.get('search'):
        url += '&text=%s' % kwargs['search']

    if kwargs.get('immo_type'):
        url += '&immo_sell_type=%s' % kwargs['immo_type']

    if kwargs.get('immo_price'):
        url += '&price=%s' % kwargs['immo_price']

    if kwargs.get('immo_size'):
        url += '&square=%s' % kwargs['immo_size']

    if kwargs.get('immo_estate'):
        url += '&real_estate_type=%s' % kwargs['immo_estate']

    logger.debug('----- Request %s' % url)
    if args.use_wget:
        filename = os.path.join(args.working_folder, 'http_response_%s_page_%s.zip' % (kwargs['postal'], kwargs['page']))
        cmd = ['wget', '--user-agent=%s' % USER_AGENT, '--header=Accept-encoding:gzip', '-O', filename, url]
        wget = subprocess.Popen(cmd, stderr=subprocess.PIPE)
        try:
            returncode = wget.wait(30)
        except subprocess.TimeoutExpired:
            wget.kill()
            raise AdRequestException('Not able to request %s. wget timeout reached.\n%s' % (url, wget.stderr.read()))
        if returncode == 0:
            try:
                zipfile = gzip.open(filename, 'rb')
                tree = html.fromstring(zipfile.read())
            except gzip.BadGzipFile:
                zipfile = open(filename, 'rb')
                tree = html.fromstring(zipfile.read())
            except Exception as e:
                raise AdRequestException(str(e))
        else:
            raise AdRequestException('Not able to request %s. Error: %s\n%s' % (url, returncode, wget.stderr.read()))
    else:
        http_session.cookies.clear()
        try:
            resp = http_session.get(url, timeout=30)
        except Exception as e:
            raise AdRequestException('Not able to request %s. An exceotion occured: %s' % (url, e))

        if resp.status_code != 200 and resp.status_code != 204:
            raise AdRequestException('Not able to request %s. Error: %s' % (url, resp.status_code))

        tree = html.fromstring(resp.text)

    json_script = tree.xpath('//script[@type="application/json" and @id="__NEXT_DATA__"]/text()')
    if not json_script:
        raise AdRequestException('Not able to get json data from %s' % tree)
    json_data = json.loads(json_script[0])

    with open(COOKIES_FILE, 'w') as f:
        json.dump(requests.utils.dict_from_cookiejar(http_session.cookies), f)

    json_props = json_data.get('props')
    json_page_props = json_props.get('pageProps')
    json_listing = json_page_props.get('searchData')
    return (
        int(json_listing.get('total')),
        int(json_listing.get('max_pages')),
        json_listing.get('ads'),
        json_data.get('runtimeConfig').get('API').get('KEY'),
        url)

def update(postal, args):
    logger.info('- Updating adverts...')
    if args.cached_cookies and os.path.exists(COOKIES_FILE):
        json_cookies = json.load(open(COOKIES_FILE))
        if json_cookies:
            http_session.cookies.update(requests.utils.cookiejar_from_dict(json_cookies))

    count_restored = 0
    count_merged = 0
    count_new = 0
    all_count_restored = 0
    all_count_merged = 0
    all_count_new = 0
    all_count_removed = 0
    found_all_ads = False

    if args.json_data_file:
        all_ads_array = json.loads(open(os.path.join(BASE_DIR, args.json_data_file), 'r').read())
        all_count_new, all_count_merged, all_count_restored = handle_ads(None, all_ads_array, args)
    else:
        all_ads_array = []
        if args.use_remote_json_api:
            total_ads, max_pages, page_ads_array, api_key, referer = find_ads(
                args,
                category=COINCOIN_CATEGORY_IMMO,
                postal=postal,
                page=1,
                search=COINCOIN_SEARCH,
                immo_type=COINCOIN_IMMO_TYPE,
                immo_price=COINCOIN_IMMO_PRICE,
                immo_size=COINCOIN_IMMO_SIZE,
                immo_estate=COINCOIN_IMMO_ESTATE)
            if page_ads_array:
                open(os.path.join(args.working_folder, 'vente_immo_%s_page_1.json' % postal), "w").write(json.dumps(page_ads_array, indent=4))
                all_ads_array = find_json_ads(api_key, referer, COINCOIN_CATEGORY_IMMO, postal, COINCOIN_SEARCH, COINCOIN_IMMO_TYPE, COINCOIN_IMMO_PRICE, COINCOIN_IMMO_SIZE)
                if all_ads_array:
                    open(os.path.join(args.working_folder, 'vente_immo_%s_all.json' % postal), "w").write(json.dumps(all_ads_array, indent=4))
                    all_count_new, all_count_merged, all_count_restored = handle_ads(None, all_ads_array, args)
                return
        else:
            page=args.update_page_offset
            page_ads_array = []
            engine = create_engine('sqlite:///coincoin%s.db' % ('_test' if args.use_test_db else ''))
            sql_session = Session(engine)
            mullvad_hosts = get_mullvad_servers()
            vpn_servers = sql_session.query(VpnServer).order_by(VpnServer.score.desc(), VpnServer.hostname.asc()).all()
            current_vpn_server = None
            vpn_servers_iter = iter(vpn_servers)
            total_pages = None
            if args.use_mullvad_vpn:
                current_vpn_server, vpn_servers_iter = pick_next_vpn_server(mullvad_hosts, sql_session, vpn_servers, vpn_servers_iter, True)
                if current_vpn_server:
                    current_vpn_server.last_used = False
                    sql_session.commit()

            while True:

                if args.update_page_limit > 0 and page > args.update_page_limit:
                    break

                if args.use_mullvad_vpn:
                    current_vpn_server, vpn_servers_iter = pick_next_vpn_server(mullvad_hosts, sql_session, vpn_servers, vpn_servers_iter, False)
                    if current_vpn_server:
                        current_vpn_server.score = 0
                        sql_session.commit()
                    else:
                        break

                try:
                    total_ads, max_pages, page_ads_array, _, _ = find_ads(
                        args,
                        category=COINCOIN_CATEGORY_IMMO,
                        postal=postal,
                        page=page,
                        search=COINCOIN_SEARCH,
                        immo_type=COINCOIN_IMMO_TYPE,
                        immo_price=COINCOIN_IMMO_PRICE,
                        immo_size=COINCOIN_IMMO_SIZE,
                        immo_estate=COINCOIN_IMMO_ESTATE)
                    if not total_pages:
                        total_pages = max_pages
                        logger.info('Found %s ads (%s pages) from %s' % (total_ads, total_pages, postal))
                    if page_ads_array:
                        open(os.path.join(args.working_folder, 'vente_immo_%s_page_%s.json' % (postal, page)), "w").write(json.dumps(page_ads_array, indent=4))
                        count_new, count_merged, count_restored = handle_ads(sql_session, page_ads_array, args)
                        all_count_new += count_new
                        all_count_merged += count_merged
                        all_count_restored += count_restored

                        if args.use_mullvad_vpn:
                            if current_vpn_server:
                                current_vpn_server.score += 1
                                current_vpn_server.last_used = False
                                sql_session.commit()
                        all_ads_array.extend(page_ads_array)
                        page += 1
                        if page > total_pages:
                            if args.update_page_offset == 1:
                                found_all_ads = True
                            break
                    else:
                        break
                except AdRequestException as e:
                    logger.debug(e)
                    if args.use_mullvad_vpn:
                        if current_vpn_server:
                            current_vpn_server.score = -1
                            current_vpn_server.last_used = False
                            blacklisted_servers.append(current_vpn_server)
                            sql_session.commit()
                    else:
                        break

            if args.use_mullvad_vpn:
                if current_vpn_server:
                    current_vpn_server.last_used = True
                    sql_session.commit()
                mullvad_disconnect()

        if all_ads_array:
            open(os.path.join(args.working_folder, 'vente_immo_%s_all.json' % postal), "w").write(json.dumps(all_ads_array, indent=4))
        else:
            raise Exception('Not able to retrieve advertisements')

    if found_all_ads:
        all_count_removed = detect_removed_adverts(postal, args, all_ads_array)

    logger.info('Handle %s adverts / New adverts: %s / Merge adverts: %s / Restored adverts: %s / Removed adverts: %s' 
        % (len(all_ads_array), all_count_new, all_count_merged, all_count_restored, all_count_removed))

    update_cache(postal, args)

    if not args.do_not_download_images:
        download_images(postal, args)

def add_advert_change(sql_session, advert, advert_change_type, advert_change_detail = None):
    advert_change = AdvertChange(
        advert_id = advert.advert_id,
        change_date = datetime.datetime.now(),
        change_type = advert_change_type,
        change_detail = advert_change_detail
    )

    logger.debug(advert_change)
    sql_session.add(advert_change)

def diff_adverts(sql_session, old_advert, advert):
    if advert.pub_date != old_advert.pub_date:
        detail_change = 'Date changed: ' + str(advert.pub_date) + ', old was: ' + str(old_advert.pub_date)
        add_advert_change(sql_session, old_advert, AdvertChangeTypeEnum.PubDateChanged, detail_change)
    if advert.subject != old_advert.subject:
        detail_change = 'Subject changed: ' + advert.subject + ', old was: ' + old_advert.subject
        add_advert_change(sql_session, old_advert, AdvertChangeTypeEnum.SubjectChanged, detail_change)
    if advert.detail != old_advert.detail:
        detail_change = ''
        for line in difflib.unified_diff(old_advert.detail.splitlines(), advert.detail.splitlines(),
            'old advert', 'new advert'):
            detail_change += line + '\n'
        add_advert_change(sql_session, old_advert, AdvertChangeTypeEnum.DetailChanged, 'Detail changed:\n' + detail_change)
    if advert.price != old_advert.price:
        detail_change = 'Price changed: ' + str(advert.price) + ', old was: ' + str(old_advert.price)
        add_advert_change(sql_session, old_advert, AdvertChangeTypeEnum.PriceChanged, detail_change)
    if advert.location != old_advert.location:
        detail_change = 'Location changed: ' + str(advert.location) + ', old was: ' + str(old_advert.location)
        add_advert_change(sql_session, old_advert, AdvertChangeTypeEnum.LocationChanged, detail_change)
    if advert.square != old_advert.square:
        detail_change  ='Square changed: ' + str(advert.square) + ', old was: ' + str(old_advert.square)
        add_advert_change(sql_session, old_advert, AdvertChangeTypeEnum.SquareChanged, detail_change)

def handle_ads(sql_session, all_ads_array, args):
    if args.do_not_use_db:
        return 0, 0, 0

    if not sql_session:
        engine = create_engine('sqlite:///coincoin%s.db' % ('_test' if args.use_test_db else ''))
        sql_session = Session(engine)

    cache_folder = os.path.join(BASE_DIR, 'cache')
    if not os.path.exists(cache_folder):
        os.mkdir(cache_folder)

    new_registered_adverts = []
    count_restored = 0
    count_merged = 0
    count_new = 0

    for json_advert in all_ads_array:
        advert_id=str(json_advert['list_id'])
        advert = Advert(
            advert_id=advert_id,
            pub_date=datetime.datetime.strptime(json_advert['first_publication_date'], '%Y-%m-%d %H:%M:%S'),
            subject=json_advert['subject'],
            detail=json_advert['body'],
            price=float(json_advert['price'][0]) if json_advert.get('price') else 0.0,
            location=json_advert['location']['zipcode'],
            square=float(next((item["value"] for item in json_advert['attributes'] if item["key"] == "square"), 0.0)),
        )

        new_registered_adverts.append(advert)
        old_advert = sql_session.query(Advert).filter(Advert.advert_id == advert_id).scalar()
        if old_advert:
            lastAdvertChange = sql_session.query(AdvertChange).filter(
                AdvertChange.advert_id == old_advert.advert_id,
            ).order_by(AdvertChange.change_date.desc()).limit(1).scalar()
            if lastAdvertChange:
                if lastAdvertChange.change_type == AdvertChangeTypeEnum.Removed:
                    add_advert_change(sql_session, old_advert, AdvertChangeTypeEnum.Restored, 'Ad restored: ' + str(old_advert))
                    count_restored += 1

            if advert != old_advert:
                logger.debug('Merge new %s and old %s' % (str(advert), str(old_advert)))
                diff_adverts(sql_session, old_advert, advert)
                sql_session.merge(advert)
                count_merged += 1
        else:
            add_advert_change(sql_session, advert, AdvertChangeTypeEnum.Added, 'New ad: ' + str(advert))
            sql_session.add(advert)
            count_new += 1
        json_images = json_advert.get('images')
        if json_images:
            json_large_images = json_images.get('urls_large')
            if json_large_images:
                for json_image_url in json_large_images:
                    old_images = None
                    if old_advert:
                        old_images = sql_session.query(AdvertImage).filter(
                            AdvertImage.advert_id == advert_id,
                            AdvertImage.url == json_image_url).all()
                        if len(old_images) > 1:
                            for old_image in old_images:
                                sql_session.delete(old_image)
                            old_images = None
                    if not old_images:
                        advert_image = AdvertImage(
                            url=json_image_url,
                            advert_id=advert_id,
                            checksum=None
                        )
                        sql_session.add(advert_image)
                        add_advert_change(sql_session, advert, AdvertChangeTypeEnum.ImageAdded, 'Added new image: ' + str(advert_image))

    sql_session.commit()

    logger.debug('Handle %s adverts / New adverts: %s / Merge adverts: %s / Restored adverts: %s' % (len(all_ads_array), count_new, count_merged, count_restored))
    return count_new, count_merged, count_restored

def detect_removed_adverts(postal, args, all_ads_array):
    logger.info('- Detect removed adverts...')

    all_count_removed = 0
    engine = create_engine('sqlite:///coincoin%s.db' % ('_test' if args.use_test_db else ''))
    sql_session = Session(engine)

    all_registered_adverts = sql_session.query(Advert).filter(Advert.location == postal).all()
    for registered_advert in all_registered_adverts:
        if not list(filter(lambda ad: str(ad['list_id']) == registered_advert.advert_id, all_ads_array)):
            lastAdvertChange = sql_session.query(AdvertChange).filter(
                AdvertChange.advert_id == registered_advert.advert_id,
            ).order_by(AdvertChange.change_date.desc()).limit(1).scalar()
            if not lastAdvertChange or lastAdvertChange.change_type != AdvertChangeTypeEnum.Removed:
                add_advert_change(sql_session, registered_advert, AdvertChangeTypeEnum.Removed, 'Ad removed: ' + str(registered_advert))
                all_count_removed += 1

    sql_session.commit()
    return all_count_removed

def update_cache(postal, args):
    logger.info('- Update adverts cache detail...')

    if args.do_not_use_db:
        return

    cache_folder = os.path.join(BASE_DIR, 'cache')
    if not os.path.exists(cache_folder):
        os.mkdir(cache_folder)

    engine = create_engine('sqlite:///coincoin%s.db' % ('_test' if args.use_test_db else ''))
    sql_session = Session(engine)

    all_registered_adverts = sql_session.query(Advert).filter(Advert.location == postal).all()
    for registered_advert in all_registered_adverts:
        location_str = registered_advert.location if registered_advert.location else 'None'
        location_cache_folder = os.path.join(cache_folder, location_str)
        if not os.path.exists(location_cache_folder):
            os.mkdir(location_cache_folder)
        advert_cache_folder = os.path.join(location_cache_folder, str(registered_advert.advert_id))
        if not os.path.exists(advert_cache_folder):
            os.mkdir(advert_cache_folder)

        with open(os.path.join(advert_cache_folder, 'detail.txt'), 'w') as f:
            f.write('Title: ' + registered_advert.subject + '\n')
            f.write('Date: ' + str(registered_advert.pub_date) + '\n')
            f.write('Location: ' + (registered_advert.location if registered_advert.location else '') + '\n')
            f.write('Square: ' + str(registered_advert.square) + '\n')
            f.write('Price: ' + str(registered_advert.price) + '\n')
            f.write('Detail: ' + registered_advert.detail + '\n')

    logger.info('Registered adverts: %s' % (str(len(all_registered_adverts))))

def detect_duplicate_images(postal, args):
    logger.info('- Detect duplicated images...')
    duplicated_count = 0
    engine = create_engine('sqlite:///coincoin%s.db' % ('_test' if args.use_test_db else ''))
    sql_session = Session(engine)
    count_images = func.count('*')
    duplicates = sql_session.query(AdvertImage.checksum, count_images) \
        .filter(AdvertImage.checksum != None) \
        .join(Advert).filter(Advert.location == postal) \
        .group_by(AdvertImage.checksum) \
        .having(count_images > 1) \
        .all()
    logger.info('Duplicate images: %s' % len(duplicates))
    for chk in duplicates:
        duplicates_ads = sql_session.query(Advert).order_by(Advert.pub_date.asc()).join(AdvertImage).filter(AdvertImage.checksum == chk[0]).all()
        previous_ad = None
        if len(duplicates_ads) > 1:
            for advert in duplicates_ads:
                if previous_ad:
                    ad_change = 'Duplicate advert detected: ' + str(advert)
                    previous_advert_changes = sql_session.query(AdvertChange).filter(
                        AdvertChange.advert_id == previous_ad.advert_id,
                        AdvertChange.change_type == AdvertChangeTypeEnum.Duplicated,
                        AdvertChange.change_detail == ad_change).all()
                    if not previous_advert_changes:
                        add_advert_change(sql_session, previous_ad, AdvertChangeTypeEnum.Duplicated, ad_change)
                        diff_adverts(sql_session, previous_ad, advert)
                        duplicated_count += 1
                previous_ad = advert
    sql_session.commit()
    logger.info('Duplicate adverts: %s' % duplicated_count)

def download_images(postal, args):
    logger.info('- Downloading missing images...')
    cache_folder = os.path.join(BASE_DIR, 'cache')
    if not os.path.exists(cache_folder):
        os.mkdir(cache_folder)
   
    engine = create_engine('sqlite:///coincoin%s.db' % ('_test' if args.use_test_db else ''))
    sql_session = Session(engine)
    all_empty_images = sql_session.query(AdvertImage) \
        .join(Advert).filter(Advert.location == postal) \
        .filter(AdvertImage.checksum==None).all()
    logger.info('Empty images checksum: ' + str(len(all_empty_images)))
    for empty_image in all_empty_images:
        location_str = empty_image.advert.location if empty_image.advert.location else 'None'
        location_cache_folder = os.path.join(cache_folder, location_str)
        if not os.path.exists(location_cache_folder):
            os.mkdir(location_cache_folder)
        image_folder = os.path.join(location_cache_folder, str(empty_image.advert_id))
        checksum = download_file(image_folder, empty_image.url, True)
        if checksum:
            empty_image.checksum = checksum
            sql_session.merge(empty_image)
    sql_session.commit()

    if all_empty_images:
        detect_duplicate_images(postal, args)

def reorder_advert_cache(args):
    cache_folder = os.path.join(BASE_DIR, 'cache')
    if not os.path.exists(cache_folder):
        os.mkdir(cache_folder)

    engine = create_engine('sqlite:///coincoin%s.db' % ('_test' if args.use_test_db else ''))
    sql_session = Session(engine)

    all_registered_adverts = sql_session.query(Advert).all()
    logger.info('Adverts count: ' + str(len(all_registered_adverts)))
    for registered_advert in all_registered_adverts:
        old_advert_cache_folder = os.path.join(cache_folder, str(registered_advert.advert_id))
        if not os.path.exists(old_advert_cache_folder):
            old_advert_cache_folder = os.path.join(cache_folder, 'None', str(registered_advert.advert_id))

        if os.path.exists(old_advert_cache_folder):
            location_str = registered_advert.location if registered_advert.location else 'None'
            location_cache_folder = os.path.join(cache_folder, location_str)
            new_advert_cache_folder = os.path.join(location_cache_folder, str(registered_advert.advert_id))
            if not os.path.exists(new_advert_cache_folder):
                if not os.path.exists(location_cache_folder):
                    os.mkdir(location_cache_folder)
                shutil.move(old_advert_cache_folder, location_cache_folder)
            elif old_advert_cache_folder != new_advert_cache_folder:
                for f in os.listdir(old_advert_cache_folder):
                    oldfile = os.path.join(old_advert_cache_folder, f)
                    logger.debug('Moving %s to %s' % (oldfile, new_advert_cache_folder))
                    if not os.path.exists(os.path.join(new_advert_cache_folder, f)):
                        shutil.move(oldfile, new_advert_cache_folder)
                    else:
                        os.remove(oldfile)
                os.removedirs(old_advert_cache_folder)

def update_vpn_servers(args):
    engine = create_engine('sqlite:///coincoin%s.db' % ('_test' if args.use_test_db else ''))
    sql_session = Session(engine)

    hosts = get_mullvad_servers()

    removed_count = 0
    db_servers = sql_session.query(VpnServer).all()
    for server in db_servers:
        if server.hostname not in hosts:
            sql_session.delete(server)
            removed_count += 1

    new_servers = []
    for host in hosts:
        server = sql_session.query(VpnServer).filter(VpnServer.hostname == host).scalar()
        if not server:
            new_server = VpnServer(
                hostname = host,
                score = 0,
                last_used = False
            )
            new_servers.append(new_server)

    sql_session.add_all(new_servers)
    sql_session.commit()

    logger.info('Added %s new servers / Removed %s old servers' % (len(new_servers), removed_count))

current_min_score = 0
def pick_next_vpn_server(mullvad_hosts, sql_session, vpn_servers, vpn_servers_iter, find_last_used):
    first_server = None
    min_score = -1
    global current_min_score
    while True:
        try:
            server = next(vpn_servers_iter)
            if first_server:
                if first_server == server:
                    error_msg = 'No more vpn server available. Loop again from %s' % server.hostname
                    logger.debug(error_msg)
                    current_min_score -= 1
                    if current_min_score < min_score:
                        raise Exception(error_msg)
            else:
                first_server = server

            if find_last_used:
                if server.last_used:
                    return server, vpn_servers_iter
            else:
                if server.hostname in mullvad_hosts:
                    if server.score >= current_min_score:
                        if server.hostname not in blacklisted_servers:
                            mullvad_disconnect()
                            if mullvad_set_relay_hostname(server.hostname) == 0:
                                logger.debug('Switching to %s vpn server' % server.hostname)
                                returncode, out, err = mullvad_connect()
                                if out:
                                    logger.debug(out)
                                if err:
                                    logger.debug(err)
                                if returncode == 0:
                                    return server, vpn_servers_iter
                                else:
                                    server.score -= 1
                                    sql_session.commit()
                                    blacklisted_servers.append(server.hostname)
                            else:
                                logger.debug('Failed to set relay server: %s' % server.hostname)
                elif not find_last_used:
                    logger.debug('Server %s not in mullvad_hosts' % server.hostname)

        except StopIteration:
            vpn_servers_iter = iter(vpn_servers)
            if find_last_used:
                break

    return None, vpn_servers_iter

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process immovable adverts.')
    parser.add_argument('--cached-cookies', action='store_true', help='Use cached cookies')
    parser.add_argument('--json-data-file', help='Test with local json data file')
    parser.add_argument('--use-test-db', action='store_true', help='Store data into test database')
    parser.add_argument('--do-not-use-db', action='store_true', help='Do not perform database operations')
    parser.add_argument('--do-not-download-images', action='store_true', help='Do not download images')
    parser.add_argument('--use-remote-json-api', action='store_true', help='Use remote json API (experimental)')
    parser.add_argument('--postal', help='Postal code to look for ad', default='69007')
    parser.add_argument('--location', help='Location to look for ad', choices=POSTAL_CODES.keys())
    parser.add_argument('--update-page-offset', help='Page offset to look for ads', type=int, default=1)
    parser.add_argument('--update-page-limit', help='Page limit to look for ads', type=int, default=0)
    parser.add_argument('--use-wget', help='Use wget for http request', action='store_false')
    parser.add_argument('--use-mullvad-vpn', help='Use mullvad vpn for http request', action='store_true')
    parser.add_argument('--working-folder', help='Output folder')
    parser.add_argument('action', help='Action to compute', choices=['update', 'update_cache', 'detect_duplicate_images',
        'download_images', 'reorder_advert_cache', 'update_vpn_servers'])
    args = parser.parse_args()
    total_start = datetime.datetime.now()
    logger.info('Start at %s' % str(total_start))
    logger.debug('Running with: %s' % args)
    try:
        if args.action == 'reorder_advert_cache':
            reorder_advert_cache(args)
        elif args.action == 'update_vpn_servers':
            update_vpn_servers(args)
        else:
            if not args.working_folder:
                date_str = datetime.datetime.now().isoformat()
                args.working_folder = os.path.join(BASE_DIR, date_str)
                
            if not os.path.exists(args.working_folder):
                os.mkdir(args.working_folder)

            file_handler = logging.FileHandler(filename=os.path.join(args.working_folder, 'work.log'))
            file_handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            file_handler.setFormatter(formatter)
            logger.addHandler(file_handler)
            postals = []
            if args.location:
                postals.extend(POSTAL_CODES.get(args.location))
            elif args.postal:
                postals.extend(args.postal.split(','))
            working_folder = args.working_folder

            if args.use_mullvad_vpn:
                update_vpn_servers(args)

            for postal in postals:
                args.working_folder = os.path.join(working_folder, str(postal))
                os.mkdir(args.working_folder)
                start = datetime.datetime.now()
                logger.info('---------------------------- %s ----------------------------' % postal)
                if args.action == 'download_images':
                    download_images(postal, args)
                elif args.action == 'detect_duplicate_images':
                    detect_duplicate_images(postal, args)
                elif args.action == 'update':
                    update(postal, args)
                elif args.action == 'update_cache':
                    update_cache(postal, args)
                duration = datetime.datetime.now() - start
                logger.info('Duration: %s' % duration)
            args.working_folder = working_folder
    except Exception as e:
        logger.error(e)
        logger.error(traceback.format_exc())
        
    logger.info('End at %s' % str(datetime.datetime.now()))
    total_duration = datetime.datetime.now() - total_start
    logger.info('Total duration: %s' % total_duration)
