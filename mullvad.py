#!/usr/bin/python3

import subprocess, re

def get_mullvad_servers():
    cmd = ['mullvad', 'relay', 'list']
    mullvad = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
    returncode = mullvad.wait()
    lines = mullvad.stdout.readlines()
    if returncode == 0:
        r = re.compile(r'(?<=^\t\t)(\w|\-)+')
        return  [r.search(hostname).group(0) for hostname in lines if r.search(hostname)]

def mullvad_set_relay_hostname(relay):
    cmd = ['mullvad', 'relay', 'set', 'hostname', relay]
    returncode = subprocess.call(cmd, stdout=subprocess.DEVNULL)
    return returncode

def mullvad_connect():
    cmd = ['mullvad', 'connect', '--wait']
    mullvad_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        out = None
        err = None
        out, err = mullvad_process.communicate(timeout=10)
        returncode = mullvad_process.returncode
    except subprocess.TimeoutExpired:
        returncode = -1
        mullvad_process.kill()

    return returncode, out, err

def mullvad_disconnect():
    cmd = ['mullvad', 'disconnect', '--wait']
    returncode = subprocess.call(cmd, stdout=subprocess.DEVNULL)
    return returncode
