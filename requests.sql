-- Prix moyen par lieu
select location, count(*), avg(price), avg(price/square) from advert group by location;

-- Prix moyen du m2 par lieu
select location, count(*), avg(price), avg(price/square) from advert where square >= 70 and square <= 90
and advert_id not in (select advert_id from advert_change where change_type='Duplicated')
group by location;

select location, count(*), avg(price), avg(price/square), avg(julianday(change_date)-julianday(pub_date)) from advert, advert_change
where advert.advert_id=advert_change.advert_id and square >= 70 and square <= 90 and change_date > date('2021-05-24')
and change_id in (select change_id from (select change_id, advert_id, change_type, change_date, row_number() over(partition by advert_id order by change_date desc ) rownum
from advert_change) where change_type='Removed' and rownum=1)
group by location;

select * from advert_change where change_type='Duplicated' and change_detail like '%price changed%' and change_detail not like '%square changed%';

select change_type, count(*) from advert_change group by change_type;

select advert.advert_id, subject, price, square from advert, advert_change
where advert.advert_id=advert_change.advert_id and location='69007' and square >= 70 and square <= 90 and pub_date > date('2021-05-24')
and change_id in (select change_id from (select change_id, advert_id, change_type, change_date, row_number() over(partition by advert_id order by change_date desc ) rownum
from advert_change) where change_type='Removed' and rownum=1);
