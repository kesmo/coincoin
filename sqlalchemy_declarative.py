from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Float, Text, Enum
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
import enum

from sqlalchemy.sql.sqltypes import Boolean

Base = declarative_base()

DATABASE_FILENAME = 'coincoin.db'

class Advert(Base):
    __tablename__ = 'advert'
    advert_id = Column(String(200), primary_key = True)
    pub_date = Column(DateTime)
    subject = Column(String(200))
    detail = Column(Text)
    price = Column(Float)
    location = Column(String(100))
    square = Column(Float)

    def __eq__(self, other):
        return (
            other.__class__ is self.__class__
            and other.advert_id == self.advert_id
            and other.pub_date == self.pub_date
            and other.subject == self.subject
            and other.detail == self.detail
            and other.price == self.price
            and other.location == self.location
            and other.square == self.square
        )

    def __str__(self):
        return "%s (%s, %s, %s, %s)" % (self.__class__.__name__, self.advert_id, self.pub_date, self.subject, self.price)


class AdvertImage(Base):
    __tablename__ = 'advert_image'
    image_id = Column(Integer, primary_key = True, autoincrement = True)
    url = Column(String(256))
    checksum = Column(String(128), index = True)
    advert_id = Column(String(200), ForeignKey('advert.advert_id'))
    advert = relationship(Advert)

    def __eq__(self, other):
        return (
            other.__class__ is self.__class__
            and other.url == self.url
            and other.checksum == self.checksum
            and other.advert_id == self.advert_id
        )

    def __str__(self):
        return "%s (%s, %s, %s)" % (self.__class__.__name__, self.advert_id, self.url, self.checksum)

class AdvertChangeTypeEnum(enum.Enum):
    Added = 1
    Removed = 2
    Restored = 3
    Duplicated = 4
    SubjectChanged = 5
    DetailChanged = 6
    PubDateChanged = 7
    PriceChanged = 8
    LocationChanged = 9
    SquareChanged = 10
    ImageAdded = 11

class AdvertChange(Base):
    __tablename__ = 'advert_change'
    change_id = Column(Integer, primary_key = True, autoincrement = True)
    advert_id = Column(String(200), ForeignKey('advert.advert_id'))
    advert = relationship(Advert)
    change_date = Column(DateTime)
    change_type = Column(Enum(AdvertChangeTypeEnum))
    change_detail = Column(Text)

    def __str__(self):
        return "%s (%s, %s, %s, %s)" % (self.__class__.__name__, self.advert_id, self.change_date, self.change_type, self.change_detail)

class VpnServer(Base):
    __tablename__ = 'vpn_server'
    hostname = Column(String(128), primary_key = True)
    score = Column(Integer)
    last_used = Column(Boolean)

engine = create_engine('sqlite:///%s' % DATABASE_FILENAME)
Base.metadata.create_all(engine)